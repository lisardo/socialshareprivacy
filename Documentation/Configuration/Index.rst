﻿.. include:: ../Includes.txt

.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.


Configuration
=============

All settings should be done via typoscript. Please refer to
ext\_typoscript\_setup.txt. All settings are commented.

You can use all settings of the original heise script via typoscript.
the typoscript is built in the same way like the
jQuery settings and all options are namend in the same way.

All settings use default values. If you want use the social media
bar with standard properties you dont need to change anything.






