.. include:: ../Includes.txt

.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.


Whats new in this version
=========================

Important changes in behavior
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- It is now possible to use the extension more than once on a single Page.
- you can change the like button of facebook to the share button
- the socialshareprivacy script from heise changed the language handling. Therfore i had to change the whole language handling too.
- the socialshareprivacy script from heise dropped some options
- the socialshareprivacy script from heise added some options which i implemented


Using more than once on a single page
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Simply use the extension as normal content element and place it on the page as often you like.
Please note: this time there is no possibilty to change the behavior of the different social media bars - all
bars on one page share all options.

I will try to change this in the next version.


Share button instead of like
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In order to change the standard like button to the share button of
facebook add the following line to your typoscript setup:

.. code-block:: typoscript

    plugin.tx_socialshareprivacy_pi1.services.facebook.sharer.status = on



Language handling
^^^^^^^^^^^^^^^^^

Socialshareprivacy now uses his own language files (in folder socialshareprivacy/socialshareprivacy/lang).
It is distributed with two: de.lang (german), en.lang (english), es.lang (espaniol), fr.lang (french) and no.lang (norvegian)
which you can use as templates for your own. The language definition is normally taken from your
language definition in your TYPO3-setup. This definition:

.. code-block:: typoscript

    config.language = de

    [globalVar = GP:L = 1]
        config.language = en
    [global]

    [globalVar = GP:L = 2]
        config.language = fr
    [global]


needs the following language files: de.lang, en.lang, fr.lang. Please do not put the language files into the
extension itself but define your own folder e.g. as subfolder of fileadmin.
For example fileadmin/plugin/socialshareprivacy/lang/.

Your folder structure:

.. code-block:: typoscript

    fileadmin
        plugin
            socialshareprivacy
                lang
                    de.lang
                    en.lang
                    fr.lang

You need to tell the extension the place of the new language files in your typoscript setup:

.. code-block:: typoscript

    plugin.tx_socialshareprivacy_pi1 {
        lang_path = /fileadmin/plugin/socialshareprivacy/lang/
        ## please note the first and last slash.
    }

**Note**

Please send me your language files - i will add them in the next releases.

Dropped / moved options
^^^^^^^^^^^^^^^^^^^^^^^

====================================== ========================
option                                 annotation
====================================== ========================
txt_help                               language file
settings_perma                         see perma_orientation
facebook:
facebook.app_id                        not necessaire any more
facebook.display_name                  language file
facebook.language                      see language handling
facebook.txt_info                      language file
facebook.txt_fb_off                    language file
facebook.txt_fb_on                     language file
twitter:
twitter.display_name                   language file
twitter.language                       see language handling
twitter.txt_info                       language file
twitter.txt_twitter_off                language file
twitter.txt_twitter_on                 language file
gplus:
gplus.display_name                     language file
gplus.language                         see language handling
gplus.txt_info                         language file
gplus.txt_gplus_off                    language file
gplus.txt_gplus_on                     language file
====================================== ========================

New options
^^^^^^^^^^^

Please refer to the file ext\_typoscript\_setup.txt in the extension folder.
You will find there all available options with annotations.

German users will find a detailed description in german language here:
`jQuery Plug-In socialshareprivacy – Dokumentation <http://www.heise.de/extras/socialshareprivacy/#options>`_