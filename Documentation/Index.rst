﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: Includes.txt
.. include:: Images.txt

.. _start:

====================================
|img-1| |img-2| Social share privacy
====================================
:Classification:
	socialshareprivacy

:Language:
	en

:Version: 
	|release|

:Author:
      Peter Linzenkirchner

:Description:
	Implements the social share privacy script from `www.heise.de
	<http://www.heise.de/>`_ : `http://www.heise.de/newsticker/meldung/Fuer-mehr-Datenschutz-Neue-Version-der-2-Klick-Empfehlungsbuttons-2101045.html
	<http://http://www.heise.de/newsticker/meldung/Fuer-mehr-Datenschutz-Neue-Version-der-2-Klick-Empfehlungsbuttons-2101045.html>`_
	relevant only for german user – because of a special law for protecting personal data which
	prohibits the normal use of facebook like button, twitter share button
	or google 1+.

:Keywords:
      facebook,twitter,google+

:Email:
      info@lisardo.de

:License:
	This document is published under the Open Content License
	available from http://www.opencontent.org/opl.shtml

:Copyright:
	2014
	
:Rendered:
	|today|


The content of this document is related to TYPO3,
a GNU/GPL CMS/Framework available from `www.typo3.org <http://www.typo3.org/>`_.

.. toctree::
   :maxdepth: 5
   :glob:

   Introduction/Index
   Update/Index
   New/Index
   UsersManual/Index
   Administration/Index
   Configuration/Index
   KnownProblems/Index
   To-doList/Index
   Changelog/Index

