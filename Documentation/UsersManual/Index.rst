﻿.. include:: ../Includes.txt
.. include:: Images.txt
.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.


Users manual
============

Installation
^^^^^^^^^^^^

Installation is very simple: 

Download and install the extension via extension manager.

Constants
^^^^^^^^^

In your main setup choose your constants editor and choose wether you
wants to install the jQuery from this extension or not:

- Load jQuery core from extension [default: 1]

Per default the jQuery Library 1.11.1 is installed from the extension
folder.

You can choose wether the javascript should included in HEAD or in the
footer of your page

- Load all javascript code in footer [default: 0]

Per default the javascript is loaded in HEAD.


TIP for Powermail-User (Version 1.x):
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If you use other extensions which needs jQuery – e. g. Powermail – you
should change the constants:

- Load jQuery core from extension: 0

- Load all javascript code in footer: 1

will work well. JQuery will be loaded from Powermail and all the other
javascript will be included in the footer of your page, so there will
be no conflict with the libraries of Powermail.

Using on page
^^^^^^^^^^^^^

After installing you will find a new element in the tab (plugins)
of your list of content elements. In order to place the social media
bar on your page make the following steps:

- new content element (same like new text or image)
- choose tab "Plugins"
- choose "social share privacy" from the list

|img-4|

**Important**: if you dont find the tab "Plugins" or the plugin
"social share privacy" is missing, check the following questions:

- is socialshareprivacy installed in your system?
- do you habe the rights to use plugins? Often non admin user dont allowed
to use plugins, but this is simply the choice of your administrator.
Please ask your admin.






