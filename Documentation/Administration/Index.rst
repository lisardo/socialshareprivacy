﻿.. include:: ../Includes.txt

.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. ==================================================
.. DEFINE SOME TEXTROLES
.. --------------------------------------------------
.. role::   underline
.. role::   typoscript(code)
.. role::   ts(typoscript)
   :class:  typoscript
.. role::   php(code)


Administration
==============

In order to use the plugin on your page, place the plugin as a normal
content element. There is nothing to configure. All options are maintained via typoscript in your setup.

Constants
^^^^^^^^^

In your main setup choose your constants editor and choose wether you
wants to install the jQuery from this extension or not:

- Load jQuery core from extension [default: 1]

Per default the jQuery Library 1.11.1 is installed from the extension
folder.

You can choose wether the javascript should included in HEAD or in the
footer of your page

- Load all javascript code in footer [default: 0]

Per default the javascript is loaded in HEAD.

Configuration
^^^^^^^^^^^^

All settings should be done via typoscript. Please refer to
ext\_typoscript\_setup.txt. All settings are commented.

You can use all settings of the original heise script via typoscript.
the typoscript is built in the same way like the
jQuery settings and all options are namend in the same way.

All settings use default values. If you want use the social media
bar with standard properties you dont need to change anything.

Connect plugin with typoscript
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Like all extensions you can place the plugin automaticall
an all pages with typoscript in your typoscript setup. See the
example for traditional marker template:

.. code-block:: typoscript

    page.10.template.marks {
        MYMARKER  < plugin.tx_socialshareprivacy_pi1
        MYMARKER {
            alignment = vertical
            perma_orientation = top
            ### etc. ###
        }
    }

or use it in a fluid template:

.. code-block:: typoscript

    <div class="someclass">
        <f:cObject typoscriptObjectPath='lib.socialshareprivacybar' />
    </div>

and put this in your typoscript setup:

.. code-block:: typoscript

    lib.socialshareprivacybar < plugin.tx_socialshareprivacy_pi1
    lib.socialshareprivacybar {
        alignment = vertical
        perma_orientation = top
        ### etc. ###
    }

it is all the same like in all other extensions which
are based on pibase (not extbase).





