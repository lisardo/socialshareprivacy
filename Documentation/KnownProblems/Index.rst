﻿.. include:: ../Includes.txt


.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.


Known problems
==============

There ist no project site in the moment. Please send a mail to
`info@lisardo.de <mailto:info@lisardo.de>`_ if you find a problem.


