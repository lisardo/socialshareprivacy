﻿.. include:: ../Includes.txt
.. include:: Images.txt

.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.


Introduction
============


What does it do?
^^^^^^^^^^^^^^^^

Implements the social share privacy script from `www.heise.de
<http://www.heise.de/>`_ : `http://www.heise.de/newsticker/meldung/Fuer-mehr-Datenschutz-Neue-Version-der-2-Klick-Empfehlungsbuttons-2101045.html
<http://http://www.heise.de/newsticker/meldung/Fuer-mehr-Datenschutz-Neue-Version-der-2-Klick-Empfehlungsbuttons-2101045.html>`_
relevant only for german user – because of a special law for protecting personal data which
prohibits the normal use of facebook like button, twitter share button
or google 1+.

Original script (download and documentation in german language): `jQuery Plug-In socialshareprivacy – Dokumentation <http://www.heise.de/extras/socialshareprivacy/>`_


Dependencies
^^^^^^^^^^^^

Socialshareprivacy should work with TYPO3 in version 4.5 up to version 6.2. It needs jQuery version 1.7 or higher (tested with 1.11.1).
You can use the embedded jQuery or your own. The extensions uses a few  Methods which are deprecated in TYPO3 6.2 in order to maintain
compatability to TYPO3 4.5. This methods will change in the next version. This version is therefore the last version which works with TYPO3 lower than 6.2.

Screenshots
^^^^^^^^^^^

Here you can see how it looks after installing:

|img-3| 

A working example you can find here: http://www.typo3-lisardo.de/



