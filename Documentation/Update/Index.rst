.. include:: ../Includes.txt

.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.


Update from earlier versions
============================

In most cases it should be possible to update the extension via
extension manager without any troubles. But the original script from heise
changed a few things: they dropped and added some. If you changed some of
the options in a fewer version you have to check wether the option is
available any more.

Please read the section "Whats new in this version".


