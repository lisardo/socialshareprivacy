﻿.. include:: ../Includes.txt


.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.



ChangeLog
=========

.. ### BEGIN~OF~TABLE ###


.. container:: table-row

   Version
         0.0.1
   
   Changes
         First upload


.. container:: table-row

   Version
         1.2.0
   
   Changes
         - Changed socialshareprivacy to version 1.4
         - facebook app-id is not needed anymore
         - default installation of jQuery can be changed via typoscript


.. container:: table-row

   Version
         1.2.1
   
   Changes
         - Compatabilty-Update for TYPO3 6.0


.. container:: table-row

   Version
         1.2.2
   
   Changes
         - english, french, spanish and norvegian implemented. Many thanks to Christian Clemens \| webzigartig <clemens@webzigartig.de>


.. container:: table-row

   Version
         1.2.4
   
   Changes
         Changed the way to include jQuery and javascript, in order to avoid
         conflicts to powermail. Now you have to change the behavior in the
         constants editor in your main setup.

.. container:: table-row

   Version
         2.0.0

   Changes
         Compatability to TYPO3 6.2. script socialshareprivacy from heise changed to version 2. Major changes e.g. in language handling.

.. container:: table-row

   Version
         2.0.1

   Changes
         Bugfixes

.. container:: table-row

   Version
         2.0.2

   Changes
         Bugfixes

.. ###### END~OF~TABLE ######


