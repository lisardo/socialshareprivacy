<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "socialshareprivacy".
 *
 * Auto generated 19-09-2014 14:41
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
	'title' => 'Social Share Privacy',
	'description' => 'script social share privacy from heise for embedding like-button, google-+-button 
and twitter according to german right',
	'category' => 'plugin',
	'version' => '2.0.2',
	'state' => 'beta',
	'uploadfolder' => 0,
	'createDirs' => '',
	'clearcacheonload' => 0,
	'author' => 'Peter Linzenkirchner',
	'author_email' => 'info@lisardo.de',
	'author_company' => '',
	'constraints' => 
	array (
		'depends' => 
		array (
			'typo3' => '4.5.0-6.2.99',
		),
		'conflicts' =>
		array (
		),
		'suggests' => 
		array (
		),
	),
);

