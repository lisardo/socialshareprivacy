<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2011 Peter Linzenkirchner <info@lisardo.de>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * [CLASS/FUNCTION INDEX of SCRIPT]
 *
 * Hint: use extdeveval to insert/update function index above.
 */

/**
 * Plugin 'social share privacy' for the 'socialshareprivacy' extension.
 *
 * @author	Peter Linzenkirchner <info@lisardo.de>
 * @package	TYPO3
 * @subpackage	tx_socialshareprivacy
 */
class tx_socialshareprivacy_pi1 extends tslib_pibase {
	var $prefixId      = 'tx_socialshareprivacy_pi1';		// Same as class name
	var $scriptRelPath = 'pi1/class.tx_socialshareprivacy_pi1.php';	// Path to this script relative to the extension dir.
	var $extKey        = 'socialshareprivacy';	// The extension key.
	var $pi_checkCHash = TRUE;
	
	/**
	 * The main method of the PlugIn
	 *
	 * @param	string		$content: The PlugIn content
	 * @param	array		$conf: The PlugIn configuration
	 * @return	The content that is displayed on the website
	 */
	function main($content, $conf) {
		$this->conf = $conf;
		$this->pi_setPiVarDefaults();
		$lConf = $this->conf;
		
		// JS Template  
		if ( isset($this->conf['template']) && ($this->conf['template'] == TRUE) ) {
			$this->tmpl = $this->cObj->fileResource($this->conf['template']);
		} else {
			$this->tmpl = $this->cObj->fileResource(t3lib_extMgm::siteRelPath($this->extKey).'template.js');
		}
        // selector
        if ( (int) $this->cObj->data['uid']) {
        	$this->selector = 'socialsharecontentid-'.$this->cObj->data['uid'];
        } else {
            $this->selector = 'socialsharepageid'.rand(1, 99999).'-'.$GLOBALS['TSFE']->id;
        }
        // skin festlegen
        $this->skin = '';
        if ($this->getSetting('skin') == 'dark') {
        	$this->skin = '_dark';
        }
        // Language for images (there are only two: de and en)
        $this->imgLanguage = '_en';
        if ($GLOBALS['TSFE']->config['config']['language'] == 'de') {
        	$this->imgLanguage = '';
        }

        // Standard Settings
        $this->options = '';
        $this->options = "
            'services' : {
                "
                .$this->getFBSettings()
                .$this->getGPSettings()
                .$this->getTwitterSettings()

                ."
            },
        ";


        $this->options .= $this->getGlobalSettings();


		$marker['###SELECTOR###'] = '#'.$this->selector;
        $marker['###OPTIONS###'] = $this->options;

		// js in template and output in frontend
		$js_conf=str_replace(array_keys($marker),$marker,$this->tmpl);
		
		if ( $this->conf['javascript_in_footer'] )  {
			$GLOBALS['TSFE']->additionalFooterData[$this->extKey.$this->selector] = '<script type="text/javascript">'.$js_conf.'</script>';
		} else {
			$GLOBALS['TSFE']->setJS($this->extKey.'-'.$this->selector,$js_conf);
		}
		// output of plugin
		$content='<div id="'.$this->selector.'" class="socialshareprivacy"></div>';
		return $content;
	}


    /**
	 * returns the settings for gPlus
	 *
	 * @return string
	 **/
	private function getFBSettings() {
        $optionString = '';
        if ($this->getSettingFB('status') == 'off' ) {
            $optionString =  "'status' : 'off'";
        } else {
            $tmpl = "'###NAME###'  : '###OPTION###',";
		    $standardOptions = array(
                'perma_option',
                'referrer_track',
                'action',
                'layout',
            );
            foreach ($standardOptions as $option ) {
                $value = $this->getSettingFB($option);
                if ($value) {
                    $marker['###NAME###'] = $option;
                    $marker['###OPTION###'] = $value;
                    $optionString .= str_replace(array_keys($marker),$marker,$tmpl) ."\n";
                }

            }

            $dummy_img = $this->getSettingFB('dummy_img');
            $action = $this->getSettingFB('action');
            if (!$dummy_img && $action == 'like') {
                $dummy_img = '/'.t3lib_extMgm::siteRelPath('socialshareprivacy').'socialshareprivacy/socialshareprivacy/images/dummy_facebook_like'.$this->skin.'.png';
            }
            if (!$dummy_img && $action != 'like') {
                $dummy_img = '/'.t3lib_extMgm::siteRelPath('socialshareprivacy').'socialshareprivacy/socialshareprivacy/images/dummy_facebook'.$this->skin.$this->imgLanguage.'.png';
            }
            $optionString .= "'dummy_img' : '".$dummy_img."'";
        }

        return "
            'facebook' : {
                "
                .$this->getFBShareSettings()
                .$optionString
                ."
            },
        ";
    }

        /**
	 * returns the settings for facebook Share
	 *
	 * @return string
	 **/
	private function getFBShareSettings() {
        $optionString = '';
        if ($this->getSettingFBShare('status') != 'on' ) {
            return "";
        }
        $optionString =  "'status' : 'on', \n";
        $dummy_img = $this->getSettingFBShare('dummy_img');
        if (!$dummy_img) {
        	$dummy_img = '/'.t3lib_extMgm::siteRelPath('socialshareprivacy').'socialshareprivacy/socialshareprivacy/images/dummy_facebook_share'.$this->skin.$this->imgLanguage.'.png';
        }
        $optionString .= "'dummy_img' : '".$dummy_img."',\n";
        $img = $this->getSettingFBShare('img');
        if (!$img) {
        	$img = '/'.t3lib_extMgm::siteRelPath('socialshareprivacy').'socialshareprivacy/socialshareprivacy/images/facebook_share'.$this->skin.$this->imgLanguage.'.png';
        }
        $optionString .= "'img' : '".$img."',\n";
        return "
            'sharer' : {
                ".$optionString."
            },
        ";
    }

    /**
	 * returns the settings for gPlus
	 *
	 * @return string
	 **/
	private function getGPSettings() {
        $optionString = '';
        if ($this->getSettingGP('status') == 'off' ) {
            $optionString =  "'status' : 'off'";
        } else {
            $tmpl = "'###NAME###'  : '###OPTION###',";
		    $standardOptions = array(
                'perma_option',
                'referrer_track',
                'size',
            );
            foreach ($standardOptions as $option ) {
                $value = $this->getSettingGP($option);
                if ($value) {
                    $marker['###NAME###'] = $option;
                    $marker['###OPTION###'] = $value;
                    $optionString .= str_replace(array_keys($marker),$marker,$tmpl) ."\n";
                }

            }

            $dummy_img = $this->getSettingGP('dummy_img');
            if (!$dummy_img) {
            	$dummy_img = '/'.t3lib_extMgm::siteRelPath('socialshareprivacy').'socialshareprivacy/socialshareprivacy/images/dummy_gplus'.$this->skin.'.png';
            }
            $optionString .= "'dummy_img' : '".$dummy_img."'";
        }

        return "
            'gplus' : {
            ".$optionString."
            },
        ";
    }

    /**
	 * returns the global settings
	 *
	 * @return string
	 **/
	private function getGlobalSettings() {
        $optionString = '';
        $tmpl = "'###NAME###'  : '###OPTION###',";
		$standardOptions = array(
            'info_link',
            'cookie_path',
            'cookie_domain',
            'cookie_expires',
            'lang_path',
            'css_path',
            'uri',
            'skin',
            'alignment',
            'switch_alignment',
            'perma_orientation',
        );

        foreach ($standardOptions as $option ) {
            $value = $this->getSetting($option);
            if ($option == 'lang_path' && empty($value) ) {
                $value = '/'.t3lib_extMgm::siteRelPath('socialshareprivacy').'socialshareprivacy/socialshareprivacy/lang/';
            }
            if ($option == 'css_path' && empty($value) ) {
                $value = '/'.t3lib_extMgm::siteRelPath('socialshareprivacy').'socialshareprivacy/socialshareprivacy/socialshareprivacy.css';
            }
            if ($value) {
                $marker['###NAME###'] = $option;
                $marker['###OPTION###'] = $value;
                $optionString .= str_replace(array_keys($marker),$marker,$tmpl) ."\n";
            }
        }
        if ($this->getSetting('language') ) {
        	$langID = $this->getSetting('language');
        } else {
            $langID = $GLOBALS['TSFE']->config['config']['language'];
        }
        $optionString .= "'language'  : '".$langID."'";
        return $optionString;
	}

    /**
	 * returns the settings for twitter
	 *
	 * @return string
	 **/
	private function getTwitterSettings() {
        $optionString = '';
        if ($this->getSettingTW('status') == 'off' ) {
            $optionString =  "'status' : 'off'";
        } else {
            $tmpl = "'###NAME###'  : '###OPTION###',";
		    $standardOptions = array(
                'perma_option',
                'referrer_track',
                'tweet_text',
                'count',
            );
            foreach ($standardOptions as $option ) {
                $value = $this->getSettingTW($option);
                if ($value) {
                    $marker['###NAME###'] = $option;
                    $marker['###OPTION###'] = $value;
                    $optionString .= str_replace(array_keys($marker),$marker,$tmpl) ."\n";
                }

            }

            $dummy_img = $this->getSettingTW('dummy_img');
            if (!$dummy_img) {
            	$dummy_img = '/'.t3lib_extMgm::siteRelPath('socialshareprivacy').'socialshareprivacy/socialshareprivacy/images/dummy_twitter'.$this->skin.'.png';
            }
            $optionString .= "'dummy_img' : '".$dummy_img."'";
        }

        return "
            'twitter' : {
            ".$optionString."
            },
        ";
    }

	
	/**
	 * returns given typoscript-value or default if typoscript empty: default settings
	 * 
	 * @param  string		setting
	 * @param  string		default value	
	 * @return void
	 **/
	private function getSetting($setting) {
		$lConf = $this->conf;

		if ( isset($lConf[$setting]) && $lConf[$setting] == TRUE) {
			return $lConf[$setting]; 
		} else {
			return '';
		}
	}
	
	/**
	 * returns given typoscript-value or default if typoscript empty: facebook settings
	 * 
	 * @param  string		setting
	 * @param  string		default value	
	 * @return void
	 **/
	private function getSettingFB($setting) {
		$lConf = $this->conf['services.']['facebook.']; 
		if ( isset($lConf[$setting]) && $lConf[$setting] == TRUE) {
			return $lConf[$setting]; 
		} else {
			return '';
		}
	}

    /**
	 * returns given typoscript-value or default if typoscript empty: facebook settings for share
	 *
	 * @param  string		setting
	 * @param  string		default value
	 * @return void
	 **/
	private function getSettingFBShare($setting) {
		$lConf = $this->conf['services.']['facebook.']['sharer.'];
		if ( isset($lConf[$setting]) && $lConf[$setting] == TRUE) {
			return $lConf[$setting];
		} else {
			return '';
		}
	}
	
	/**
	 * returns given typoscript-value or default if typoscript empty: twitter settings
	 * 
	 * @param  string		setting
	 * @param  string		default value	
	 * @return void
	 **/
	private function getSettingTW($setting) {
		$lConf = $this->conf['services.']['twitter.']; 
		if ( isset($lConf[$setting]) && $lConf[$setting] == TRUE) {
			return $lConf[$setting]; 
		} else {
			return '';
		}
	}
	
	/**
	 * returns given typoscript-value or default if typoscript empty: Google+-Settings
	 * 
	 * @param  string		setting
	 * @param  string		default value	
	 * @return void
	 **/
	private function getSettingGP($setting) {
		$lConf = $this->conf['services.']['gplus.']; 
		if ( isset($lConf[$setting]) && $lConf[$setting] == TRUE) {
			return $lConf[$setting]; 
		} else {
			return '';
		}
	}
	
}



if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/socialshareprivacy/pi1/class.tx_socialshareprivacy_pi1.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/socialshareprivacy/pi1/class.tx_socialshareprivacy_pi1.php']);
}

?>